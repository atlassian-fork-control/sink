package metrics

import (
	"time"

	"github.com/DataDog/datadog-go/statsd"

	l "bitbucket.org/atlassian/sink/logger"
)

// Metrics is an API interface for metrics.
type Metrics interface {
	Count(key string, tags ...string)
	CountValue(key string, value int, tags ...string)
	Duration(key string, duration time.Duration, tags ...string)
}

// New creates new instance of Metrics that uses StatsD.
func New(address, appName string) (Metrics, error) {
	c, err := statsd.New(address)
	if err != nil {
		return new(NoOpMetrics), err
	}
	// prefix every metric with the app name
	c.Namespace = appName + "."

	l.Log.WithFields("address", address).Info("connected to StatsD")

	return &StatsDMetrics{client: c}, nil
}

// StatsDMetrics is a wrapper around StatsD client.
type StatsDMetrics struct {
	client *statsd.Client
}

// Count adds +1 to the provided key in DataDog.
func (metrics *StatsDMetrics) Count(key string, tags ...string) {
	err := metrics.client.Count(key, 1, tags, 1)
	if err != nil {
		l.Log.Warn("error sending count for %s %v: %v", key, tags, err)
	}
}

// CountValue adds given value to the provided key in DataDog.
func (metrics *StatsDMetrics) CountValue(key string, value int, tags ...string) {
	err := metrics.client.Count(key, int64(value), tags, 1)
	if err != nil {
		l.Log.Warn("error sending count for %s %v: %v", key, tags, err)
	}
}

// Duration sends duration for the provided key to DataDog.
func (metrics *StatsDMetrics) Duration(key string, duration time.Duration, tags ...string) {
	err := metrics.client.Timing(key, duration, tags, 1)
	if err != nil {
		l.Log.Warn("error sending duration for %s %v: %v", key, tags, err)
	}
}

// NoOpMetrics no op implementation of Metrics interface.
// Used in tests and local dev loop.
type NoOpMetrics struct {
}

// Count - nop.
func (metrics *NoOpMetrics) Count(key string, tags ...string) {
	// No op
}

// CountValue - nop.
func (metrics *NoOpMetrics) CountValue(key string, value int, tags ...string) {
	// No op
}

// Duration - nop.
func (metrics *NoOpMetrics) Duration(key string, duration time.Duration, tags ...string) {
	// No op
}
